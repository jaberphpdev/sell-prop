<div class="fixed w-full py-4 px-12 flex justify-between items-center z-30" id="header_area">
    <div class="min-w-max">
        <a href="{{ url('/')  }}">
            <img src="/img/logo.jpeg" width="100" class="text-blue-500" alt="Logo">
        </a>
    </div>
    <div class="w-full">
        <ul class="flex justify-center">
            <li><a class="inline-block p-4 text-white" href="#">Land</a></li>
            <li><a class="inline-block p-4 text-white" href="#">Villa</a></li>
            <li><a class="inline-block p-4 text-white" href="#">Appertment</a></li>
            <li><a class="inline-block p-4 text-white" href="#">About Us</a></li>
            <li><a class="inline-block p-4 text-white" href="#">Contact Us</a></li>
        </ul>
    </div>
    <div class="min-w-max text-3xl">
        <a href="">🇺🇸</a>
        <a href="">🇹🇷</a>
    </div>
</div>